# Practice-two

## Overview

- This document provides the requirement for HTML CSS practice two.
- Design: [Figma](<https://www.figma.com/file/BcGKbtsxxVx4uXOxgZAZcL/Food-delivery-website-(Community)?node-id=803%3A5>)

## Technical

- HTML / CSS / SASS(SCSS)

## Timeline

- 8 days

## Teamsize

- 1 dev

## Editor

- Visual Studio Code

## REQUIREMENTS

- Work fine on Chrome browser latest version
- Use the right HTML tags
- Apply Flexbox
- Apply CSS Guideline
- Apply Technical parcel
- Upgrade the existing practice to responsive
- Work fine on Chrome browser latest version
- Getting the code to work cross-browsers latest version (Chrome, Firefox, MS Edge, Opera, Safari)


## Targets

- Apply knowledge to responsive practice one design
- Used media queries for popular screen size

### How to run practice two:

- $ git clone --branch feature/practice-two https://gitlab.com/hungmq1/html-css.git

- cd html-css/practice/practice-two

 > npm install ( yarn install )

 > npm run start ( yarn start )

follow at: 

<a href="http://localhost:1234/">http://localhost:1234/</a>
